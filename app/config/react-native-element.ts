import { ButtonProps, CheckBoxProps, InputProps } from "react-native-elements"
import { TextStyle } from "react-native"
import { color, typography } from "../theme"

const PRIMARY_FONT_FAMILY: TextStyle = {
  fontFamily: typography.primary,
}

export const ReactNativeElementTheme = {
  colors: {
    primary: color.primary,
  },
  Button: {
    buttonStyle: {
      borderRadius: 5,
    },
    titleStyle: PRIMARY_FONT_FAMILY,
  } as ButtonProps,
  Input: {
    style: PRIMARY_FONT_FAMILY,
  } as InputProps,
  CheckBox: {
    titleProps: {
      style: PRIMARY_FONT_FAMILY,
    },
  } as CheckBoxProps,
}
