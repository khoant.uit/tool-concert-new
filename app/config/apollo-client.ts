import { ApolloClient, createHttpLink, InMemoryCache } from "@apollo/client"
import { setContext } from "@apollo/client/link/context"
import { onError } from "@apollo/client/link/error"
import { loadString } from "../utils/storage"

const httpLink = createHttpLink({
  uri: "https://us-central1-tool-concert-stag.cloudfunctions.net/api/graphql/",
})
const authLink = setContext(async (_, { headers }) => {
  let token = await loadString("access_token")
  return {
    headers: {
      ...headers,
      Authorization: token ? `Bearer ${token}` : "",
    },
  }
})
const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors)
    graphQLErrors.map(({ message, locations, path, extensions }) => {
      console.tron.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
      )
      if (extensions?.exception?.response?.statusCode === 401) {
        // do something please
      }
    })

  if (networkError) console.log(`[Network error]: ${networkError}`)
})
export const apolloClient = new ApolloClient({
  link: authLink.concat(errorLink).concat(httpLink),
  cache: new InMemoryCache(),
})
