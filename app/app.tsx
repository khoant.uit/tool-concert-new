/**
 * Welcome to the main entry point of the app. In this file, we'll
 * be kicking off our app.
 *
 * Most of this file is boilerplate and you shouldn't need to modify
 * it very often. But take some time to look through and understand
 * what is going on here.
 *
 * The app navigation resides in ./app/navigators, so head over there
 * if you're interested in adding screens and navigators.
 */
import "./i18n"
import "./utils/ignore-warnings"
import React, { useState, useEffect, useRef } from "react"
import { NavigationContainerRef } from "@react-navigation/native"
import { SafeAreaProvider, initialWindowMetrics } from "react-native-safe-area-context"
import { initFonts } from "./theme/fonts" // expo
import * as storage from "./utils/storage"
import {
  useBackButtonHandler,
  RootNavigator,
  canExit,
  setRootNavigation,
  useNavigationPersistence,
} from "./navigators"
import { AuthProvider } from "./contexts/AuthContext"
import { RootStore, RootStoreProvider, setupRootStore } from "./models"
import { ToggleStorybook } from "../storybook/toggle-storybook"
// This puts screens in a native ViewController or Activity. If you want fully native
// stack navigation, use `createNativeStackNavigator` in place of `createStackNavigator`:
// https://github.com/kmagiera/react-native-screens#using-native-stack-navigator
import { enableScreens } from "react-native-screens"
import { ApolloProvider } from "@apollo/client"
import { ThemeProvider } from "react-native-elements"
import { ReactNativeElementTheme } from "./config/react-native-element"
import FlashMessage from "react-native-flash-message"
import { View } from "react-native"
import "core-js/features/promise"
import { apolloClient } from "./config/apollo-client"

enableScreens()

export const NAVIGATION_PERSISTENCE_KEY = "NAVIGATION_STATE"

/**
 * This is the root component of our app.
 */
function App() {
  const navigationRef = useRef<NavigationContainerRef>()
  const [rootStore, setRootStore] = useState<RootStore | undefined>(undefined)

  setRootNavigation(navigationRef)
  useBackButtonHandler(navigationRef, canExit)
  const { initialNavigationState, onNavigationStateChange } = useNavigationPersistence(
    storage,
    NAVIGATION_PERSISTENCE_KEY,
  )

  // Kick off initial async loading actions, like loading fonts and RootStore
  useEffect(() => {
    ;(async () => {
      await initFonts() // expo
      setupRootStore().then(setRootStore)
    })()
  }, [])

  // Before we show the app, we have to wait for our state to be ready.
  // In the meantime, don't render anything. This will be the background
  // color set in native by rootView's background color. You can replace
  // with your own loading component if you wish.
  if (!rootStore) return null

  // otherwise, we're ready to render the app
  return (
    <ApolloProvider client={apolloClient}>
      <AuthProvider>
        <ThemeProvider theme={ReactNativeElementTheme}>
          <ToggleStorybook>
            <RootStoreProvider value={rootStore}>
              <SafeAreaProvider initialMetrics={initialWindowMetrics}>
                <View style={{ flex: 1 }}>
                  <RootNavigator
                    ref={navigationRef}
                    initialState={initialNavigationState}
                    onStateChange={onNavigationStateChange}
                  />
                  {/* GLOBAL FLASH MESSAGE COMPONENT INSTANCE */}
                  <FlashMessage position="bottom" />
                  {/* <--- here as last component */}
                </View>
              </SafeAreaProvider>
            </RootStoreProvider>
          </ToggleStorybook>
        </ThemeProvider>
      </AuthProvider>
    </ApolloProvider>
  )
}

export default App
