import { Platform, TextStyle } from "react-native"
import { color, typography } from "../../theme"

/**
 * All text will start off looking like this.
 */
const BASE: TextStyle = {
  fontFamily: typography.primary,
  color: color.text,
  fontSize: 15,
}

/**
 * All the variations of text styling within the app.
 *
 * You want to customize these to whatever you need in your app.
 */
export const presets = {
  /**
   * The default text styles.
   */
  default: BASE,

  /**
   * A bold version of the default text.
   */
  bold: { ...BASE, fontWeight: "bold" } as TextStyle,

  /**
   * Large headers.
   */
  header: {
    ...BASE,
    fontSize: 18,
    lineHeight: 28,
    fontWeight: "700",
    fontStyle: "normal",
    textAlign: "center",
    color: color.palette.white,
  } as TextStyle,

  /**
   * Field labels that appear on forms above the inputs.
   */
  fieldLabel: {
    ...BASE,
    fontWeight: "700",
    fontSize: 14,
    lineHeight: 28,
    color: color.dim,
  } as TextStyle,

  /**
   * A smaller piece of secondard information.
   */
  secondary: { ...BASE, fontSize: 9, color: color.dim } as TextStyle,
  /**
   * A smaller piece of secondard information.
   */
  error: { ...BASE, fontSize: undefined, color: color.error } as TextStyle,
}

/**
 * A list of preset names.
 */
export type TextPresets = keyof typeof presets
