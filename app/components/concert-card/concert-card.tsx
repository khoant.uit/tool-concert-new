import { useNavigation } from "@react-navigation/core"
import React, { useState } from "react"
import { View, Image, TextStyle } from "react-native"
import { useAuth } from "../../hooks/useAuth"
import { Button, Text } from ".."
import { EyeIcon } from "../all-svg-icons/all-svg-icons"
import { ViewItem, Wrapper } from "./concert-card.styled"
import { ButtonProps } from "../button/button.props"

const BUTTON_TITLE: TextStyle = {
  fontSize: 8,
  fontWeight: "500",
  lineHeight: 9.75,
}
const SUB_TEXT: TextStyle = {
  fontSize: 11,
}

const ButtonAddToCart = (props: ButtonProps) => <Button titleStyle={BUTTON_TITLE} {...props} />

type props = {
  onPress?: (e) => void
  name: string
  place: string
  timeFrom: string
  ownerName: string
  views: number
  createAt: string
}

export function ConcertCard({ onPress, name, place, timeFrom, ownerName, views, createAt }: props) {
  const { role } = useAuth()
  const [focus, setFocus] = useState(false)
  const navigation = useNavigation()
  return (
    <Wrapper onPress={() => navigation.navigate("concertDetail")}>
      <View style={{ aspectRatio: 2.65 }}>
        {/* <Button title="View" onPress={onPress} /> */}
        <Image
          style={{
            flex: 1,
            resizeMode: "contain",
            width: undefined,
            height: undefined,
            // marginLeft: 'auto',
            // marginRight: 'auto',
          }}
          //   source={require("./../../images/ConcertDefault.png")}
          source={require("./../../images/ConcertDefault.png")}
        />
      </View>
      <View
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-around",
          alignItems: "center",
          flexWrap: "wrap",
        }}
      >
        <ViewItem>
          <Text preset="bold">{name}</Text>
        </ViewItem>
        <ViewItem>
          <Text preset="bold" style={{ textAlign: "center" }}>
            {timeFrom}
          </Text>
        </ViewItem>
        <ViewItem>
          <Text preset="bold" style={{ textAlign: "right" }}>
            {place}
          </Text>
        </ViewItem>
        <ViewItem
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Image style={{ width: 27, height: 27 }} source={require("./../../images/avt.png")} />
          <View style={{ marginLeft: 5 }}>
            <Text preset="bold" style={SUB_TEXT}>
              {ownerName}
            </Text>
            <Text preset="bold" style={SUB_TEXT}>
              {createAt}
            </Text>
          </View>
        </ViewItem>
        <ViewItem>
          <ButtonAddToCart
            title={!focus ? "Add to favorite" : "Remove from favorite"}
            onPress={() => {
              role === "guest" ? navigation.navigate("login") : setFocus(!focus)
            }}
            preset={focus ? "primary" : "outline"}
          />
        </ViewItem>
        <ViewItem
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-end",
            alignItems: "center",
          }}
        >
          <EyeIcon />
          <Text
            preset="bold"
            style={[
              SUB_TEXT,
              {
                textAlign: "right",
                marginLeft: 3,
              },
            ]}
          >
            {views}
          </Text>
        </ViewItem>
      </View>
    </Wrapper>
  )
}
