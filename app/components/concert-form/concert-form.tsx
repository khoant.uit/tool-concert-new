import * as React from "react"
import { View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color } from "../../theme"
import { Button, DropDownIcon, Input, Text } from "../"
import { Calendar, Wrapper } from "./concert-form.styled"
import DropDownPicker from "react-native-dropdown-picker"
import { useState } from "react"
import DatePicker from "react-native-date-picker"
import { Overlay } from "react-native-elements"
import { useCategoriesQuery } from "../../graphql/gen-types"
import { Controller } from "react-hook-form"
export interface ConcertFormProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  control: any
  getValues: any
}

/**
 * Describe your component here
 */
export const ConcertForm = observer(function ConcertForm(props: ConcertFormProps) {
  const { style, control, getValues } = props
  const [show, setShow] = useState(false)
  const { data } = useCategoriesQuery({
    onError: (err) => console.tron.logImportant(err),
  })
  const categoryList = data?.categories.map((item) => ({ label: item.name, value: item.id }))

  // const onChange = (selectedDate) => {
  //   const currentDate = selectedDate || date
  //   setDate(currentDate)
  //   console.tron.log(currentDate)
  // }

  return (
    // <View style={[CONTAINER, style]}>
    //   <Text style={TEXT}>Hello</Text>
    // </View>
    <View style={{ flex: 1 }}>
      <Wrapper>
        {/* <TextTitle>Name of concert</TextTitle> */}
        <Text preset="fieldLabel">Name of concert</Text>
        <Controller
          control={control}
          render={({ field: { value, onBlur, onChange } }) => (
            <Input placeholder="Name" value={value} onBlur={onBlur} onChangeText={onChange} />
          )}
          name="name"
        />
        {/* <TextTitle>Place</TextTitle> */}
        <Text preset="fieldLabel">Place</Text>
        <Controller
          control={control}
          render={({ field: { value, onBlur, onChange } }) => (
            <Input placeholder="Place" value={value} onBlur={onBlur} onChangeText={onChange} />
          )}
          name="place"
        />
        {/* <TextTitle>Time From</TextTitle> */}
        <Text preset="fieldLabel">Time From</Text>
        <Controller
          control={control}
          render={({ field: { value, onBlur, onChange } }) => (
            <Calendar onPress={() => setShow(true)} activeOpacity={0.6}>
              <Text>{getValues("timeFrom")?.toLocaleString()}</Text>
              <Overlay isVisible={show} onBackdropPress={() => setShow(false)}>
                <DatePicker
                  date={value}
                  onDateChange={(date) => {
                    onChange(date)
                    console.tron.log(date)
                  }}
                  minuteInterval={5}
                  mode="datetime"
                />
                <Button title="Confirm" onPress={() => setShow(false)} />
              </Overlay>
            </Calendar>
          )}
          defaultValue={new Date()}
          name="timeFrom"
        />
        {/* <TextTitle>Time To</TextTitle> */}
        <Text preset="fieldLabel">Time To</Text>
        <Controller
          control={control}
          render={({ field: { value, onBlur, onChange } }) => (
            <Calendar onPress={() => setShow(true)} activeOpacity={0.6}>
              <Text>{getValues("timeTo")?.toLocaleString()}</Text>
              <Overlay isVisible={show} onBackdropPress={() => setShow(false)}>
                <DatePicker
                  date={value}
                  onDateChange={(date) => {
                    onChange(date)
                    console.tron.log(date)
                  }}
                  minuteInterval={5}
                  mode="datetime"
                />
                <Button title="Confirm" onPress={() => setShow(false)} />
              </Overlay>
            </Calendar>
          )}
          defaultValue={new Date()}
          name="timeTo"
        />
      </Wrapper>
      <Wrapper>
        {/* <TextTitle>Category</TextTitle> */}
        <Text preset="fieldLabel">Category</Text>
        <Controller
          control={control}
          render={({ field: { value, onBlur, onChange } }) => (
            <DropDownPicker
              placeholder="Select a category"
              placeholderStyle={{ color: "#A9A9A9" }}
              style={{ borderColor: color.primary }}
              onChangeItem={(value) => {
                console.tron.log(value)
                onChange(value)
              }}
              items={categoryList}
              itemStyle={{
                justifyContent: "flex-start",
              }}
              globalTextStyle={{ color: "#A9A9A9", fontWeight: "700" }}
              dropDownStyle={{
                borderColor: color.primary,
                borderWidth: 1,
              }}
              customArrowDown={() => <DropDownIcon />}
              customArrowUp={() => <DropDownIcon />}
            />
          )}
          name="categoryIds"
        />

        {/* <TextTitle>Description</TextTitle> */}
        <Text preset="fieldLabel">Description</Text>
        <Controller
          control={control}
          render={({ field: { value, onBlur, onChange } }) => (
            <Input
              placeholder="Description"
              value={value}
              onBlur={onBlur}
              onChangeText={onChange}
            />
          )}
          name="desicription"
        />
        {/* <TextTitle>Password for a judge</TextTitle> */}
        <Text preset="fieldLabel">Password for a judge</Text>
        <Controller
          control={control}
          render={({ field: { value, onBlur, onChange } }) => (
            <Input placeholder="Password" value={value} onBlur={onBlur} onChangeText={onChange} />
          )}
          name="secretKey"
        />
      </Wrapper>
    </View>
  )
})
