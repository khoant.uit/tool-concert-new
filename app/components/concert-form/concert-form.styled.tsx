import styled from "styled-components/native"
import { color } from "../../theme"

const Wrapper = styled.View`
  width: 100%;
  background: rgba(228, 195, 154, 0.1);
  padding-left: 33px;
  padding-right: 33px;
  padding-bottom: 22px;
  margin-top: 15px;
`

const Input = styled.TextInput`
  background: #ffffff;
  border: 1px solid ${color.primary};
  border-radius: 5px;
  margin-top: 8px;
  height: 37px;
  padding-left: 10px;
`

const Calendar = styled.TouchableOpacity`
  background: #ffffff;
  border: 1px solid ${color.primary};
  border-radius: 5px;
  /* margin-top: 8px; */
  height: 37px;
  padding-left: 10px;
  justify-content: center;
`

export { Wrapper, Input, Calendar }
