import React, { useState } from "react"
import { TouchableOpacity, TextStyle, ViewStyle } from "react-native"
import { Wrapper, TextTitle, RememberContainer, SubText, TextError } from "./login-form.styled"
import { CheckBox } from "react-native-elements"
import { useNavigation } from "@react-navigation/native"
import { useAuth } from "../../hooks/useAuth"
import { Button, EyePasswordIcon, Input } from ".."
import { color } from "../../theme"
import { useForm, Controller } from "react-hook-form"
import auth from "@react-native-firebase/auth"
import { useUsersLazyQuery } from "../../graphql/gen-types"
import { saveString } from "../../utils/storage"
import { showMessage } from "react-native-flash-message"

const RIGHT_ICON_CONTAINER: ViewStyle = {
  height: "auto",
  paddingRight: 0,
}

export function LoginForm() {
  const [firebaseAuthLoading, setFirebaseAuthLoading] = useState(false)
  let token
  const navigation = useNavigation()
  const { SIGNIN_SUCCESS } = useAuth()
  const [getUserInfo, { loading }] = useUsersLazyQuery({
    onCompleted: async (data) => {
      await SIGNIN_SUCCESS({ access_token: token, role: data.user.role })
      showMessage({
        message: `Welcome back, ${data.user.displayName}!`,
        type: "success",
      })
      navigation.navigate("home")
    },
    onError: (err) => {
      console.tron.log(err)
      showMessage({ message: err.message, type: "danger" })
    },
  })
  const {
    control,
    handleSubmit,
    formState: { errors, isValid, isValidating, isSubmitting },
  } = useForm()

  const handleAuthenticated = (data) => {
    setFirebaseAuthLoading(true)
    auth()
      .signInWithEmailAndPassword(data.username, data.password)
      .then(async (data) => {
        setTimeout(() => {
          setFirebaseAuthLoading(false)
        }, 500)
        console.tron.log("login firebase success")
        console.tron.log("user uid: " + data.user.uid)
        token = await data.user.getIdToken()
        saveString("access_token", token)
        getUserInfo({ variables: { id: data.user.uid } })
      })
      .catch((err) => {
        showMessage({ message: "Invalid username or password", type: "danger", autoHide: false })
        setTimeout(() => {
          setFirebaseAuthLoading(false)
        }, 500)
      })
  }

  return (
    <Wrapper>
      <TextTitle style={{ marginTop: 20 }}>User name</TextTitle>
      <Controller
        control={control}
        render={({ field: { onBlur, onChange, value } }) => (
          <Input
            placeholder="User name"
            value={value}
            onBlur={onBlur}
            onChangeText={onChange}
            autoCompleteType="username"
          />
        )}
        name="username"
        rules={{ required: true }}
        defaultValue=""
      />
      <TextError style={{ color: errors.username ? "red" : color.palette.white }}>
        This field is required
      </TextError>
      <TextTitle style={{ marginTop: 8 }}>Password</TextTitle>
      <Controller
        control={control}
        render={({ field: { onBlur, onChange, value } }) => (
          <Input
            rightIcon={EyePasswordIcon}
            rightIconContainerStyle={RIGHT_ICON_CONTAINER}
            placeholder="Password"
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
            secureTextEntry={true}
            autoCompleteType="password"
          />
        )}
        name="password"
        rules={{ required: true }}
      />
      <TextError style={{ color: errors.password ? "red" : color.palette.white }}>
        This field is required
      </TextError>
      <RememberContainer>
        <CheckBox checked checkedColor={color.primary} size={22} uncheckedColor={color.primary} />
        <SubText style={{ color: "#A9A9A9" }}>Remember me</SubText>
      </RememberContainer>
      <Button
        title="Log in"
        preset="largePrimary"
        loading={loading || firebaseAuthLoading}
        onPress={handleSubmit(handleAuthenticated)}
      />
      <TouchableOpacity
        style={{
          alignItems: "center",
          justifyContent: "center",
          width: "100%",
          marginTop: 10,
        }}
      >
        <SubText style={{ color: "#C9C9C9" }}>Can't log in?</SubText>
      </TouchableOpacity>
      <Button
        title="Sign up"
        preset="largeOutline"
        onPress={() => {
          navigation.navigate("signup")
        }}
      />
      {/* 
      <Button
        title="get token"
        onPress={() => {
          loadString("access_token").then((data) => console.tron.log(data))
        }}
      />
      <Button
        title="clear token"
        onPress={() => {
          remove("access_token")
        }}
      />
      <Button
        title="call api user"
        onPress={() => {
          getUserInfo({ variables: { id: "v5BYaOQCSNc3CD2OdGwyErQeiMI2" } })
        }}
      /> */}
    </Wrapper>
  )
}
