export const icons = {
  back: require("./back.png"),
  bullet: require("./bullet.png"),
  plus: require("./plus.png")
}

export type IconTypes = keyof typeof icons
