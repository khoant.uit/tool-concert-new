import * as React from "react"
import { ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, spacing } from "../../theme"
import { Input as RNEInput, InputProps as RNEInputProps } from "react-native-elements"

const CONTAINER: ViewStyle = {
  paddingHorizontal: 0,
  justifyContent: "center",
}
const INPUT_CONTAINER: ViewStyle = {
  backgroundColor: color.palette.white,
  borderWidth: 1,
  borderRadius: spacing[1],
  paddingHorizontal: 10,
  borderColor: color.primary,
}

export interface InputProps extends RNEInputProps {
  /**
   * An optional style override useful for padding & margin.
   */
  // style?: ViewStyle
}

/**
 * Describe your component here
 */
export const Input = observer(function Input(props: InputProps) {
  // const { style } = props

  return (
    <RNEInput
      placeholderTextColor={color.dim}
      containerStyle={CONTAINER}
      inputContainerStyle={INPUT_CONTAINER}
      renderErrorMessage={false}
      {...props}
    />
  )
})
