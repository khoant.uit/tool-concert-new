import React, { useEffect, useState } from "react"
import { Input, RadioButton, Text as RawText, Button } from ".."
import { Wrapper, TextNote } from "./signup-form.styled"
import { useRegisterUserMutation } from "../../graphql/gen-types"
import { TextStyle, View, Text } from "react-native"
import { useForm, Controller } from "react-hook-form"
import { color } from "../../theme"
import { showMessage } from "react-native-flash-message"
import { useNavigation } from "@react-navigation/native"

const INPUT_LABEL: TextStyle = {
  fontWeight: "700",
  fontSize: 14,
  lineHeight: 28,
  color: color.dim,
  // marginVertical: 10,
}

const InputGender = ({ value, onChange }) => {
  const [selectedIndex, setSelectedIndex] = useState<"male" | "female" | undefined>(value)
  const updateIndex = (selectedIndex) => {
    setSelectedIndex(selectedIndex)
  }
  useEffect(() => {
    value !== "" && setSelectedIndex(value)
    console.tron.log({ value })
  }, [value])
  return (
    <View
      style={{
        flex: 1,
        flexDirection: "row",
        width: "100%",
        // borderWidth: 1,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <RadioButton
        checked={selectedIndex === "male"}
        title="Male"
        onPress={() => {
          updateIndex(0)
          onChange("male")
        }}
      />
      <RadioButton
        checked={selectedIndex === "female"}
        title="Female"
        onPress={() => {
          updateIndex(1)
          onChange("female")
        }}
      />
      <RadioButton
        checked={selectedIndex === undefined}
        title="Undefined"
        onPress={() => {
          updateIndex(2)
          onChange(undefined)
        }}
      />
    </View>
  )
}

const LabelRequire = ({ children }) => (
  <Text style={INPUT_LABEL}>
    {children} (
    <Text
      style={{
        color: "red",
        fontSize: 15,
      }}
    >
      *
    </Text>
    )
  </Text>
)

export function SignupForm() {
  const navigation = useNavigation()
  const [registerUser] = useRegisterUserMutation({
    onCompleted: (data) => {
      console.tron.log(data)
      showMessage({
        message: "Signup success!, please use this account to login",
        description: "This is our second message",
        type: "success",
      })
      navigation.navigate("login")
    },
    onError: (error) => {
      console.tron.log(error)
      showMessage({
        message: "Signup error! please check input again",
        description: "This is our second message",
        type: "danger",
      })
    },
  })
  const [errorMessage, setErrorMessage] = useState(
    "Please fill out all the require fields and try again.",
  )
  const [hasError, setHasError] = useState(false)

  const {
    control,
    handleSubmit,
    formState: { errors, isValid, isValidating, isSubmitting },
    getValues,
  } = useForm()

  useEffect(() => {
    console.tron.log({ errors })
    // handleValidate()
  }, [errors, isValidating, isSubmitting])

  const handleSignup = (data) => {
    console.tron.log(data)
    if (data.password !== data.passwordConfirm) {
      setErrorMessage("Password confirm does not match!")
    }
    const { displayName, gender, email, password, companyName, age, phoneNumber } = data
    registerUser({
      variables: {
        input: {
          email,
          password,
          displayName,
          companyName,
          age: Number(age),
          phoneNumber,
          gender,
        },
      },
    })
  }
  // const handleValidate: () => boolean = () => {
  //   if (Object.keys(errors).length !== 0) {
  //     setHasError(true)
  //     setErrorMessage("Please fill out all the require fields and try again.")
  //     return false
  //   }
  //   if (getValues("password") !== getValues("passwordConfirm")) {
  //     console.tron.log({ pass: getValues("password"), confirm: getValues("passwordConfirm") })
  //     setHasError(true)
  //     setErrorMessage("Confirm password does not match!")
  //     return false
  //   }
  //   return true
  // }

  return (
    <Wrapper>
      <Controller
        control={control}
        render={({ field: { onBlur, onChange, value } }) => (
          <Input
            label={<LabelRequire>Display Name</LabelRequire>}
            labelStyle={INPUT_LABEL}
            placeholder="Display Name"
            value={value}
            onBlur={onBlur}
            onChangeText={onChange}
            autoCompleteType="off"
          />
        )}
        name="displayName"
        rules={{ required: true }}
        defaultValue=""
      />
      <LabelRequire>Gender</LabelRequire>
      <Controller
        control={control}
        render={({ field: { onBlur, onChange, value } }) => (
          <InputGender value={value} onChange={onChange} />
        )}
        name="gender"
        defaultValue={undefined}
      />
      <Controller
        control={control}
        render={({ field: { onBlur, onChange, value } }) => (
          <Input
            label={<LabelRequire>Email</LabelRequire>}
            placeholder="email@mail.com"
            value={value}
            onBlur={onBlur}
            onChangeText={onChange}
            autoCompleteType="email"
          />
        )}
        name="email"
        defaultValue=""
      />
      <Controller
        control={control}
        render={({ field: { onBlur, onChange, value } }) => (
          <Input
            label={<LabelRequire>Password</LabelRequire>}
            placeholder="password"
            value={value}
            onBlur={onBlur}
            onChangeText={onChange}
            autoCompleteType="password"
            secureTextEntry={true}
          />
        )}
        name="password"
        rules={{ required: true }}
        defaultValue=""
      />
      <Controller
        control={control}
        render={({ field: { onBlur, onChange, value } }) => (
          <Input
            label={<LabelRequire>Password (confirm)</LabelRequire>}
            placeholder="confirm your password"
            value={value}
            onBlur={onBlur}
            onChangeText={onChange}
            autoCompleteType="password"
            secureTextEntry={true}
          />
        )}
        name="passwordConfirm"
        rules={{
          required: true,
          // validate: () => {
          //   if (getValues("password") !== getValues("passwordConfirm")) {
          //     return false
          //   } else {
          //     return true
          //   }
          // },
        }}
        defaultValue=""
      />
      <Controller
        control={control}
        render={({ field: { onBlur, onChange, value } }) => (
          <Input
            label="Company"
            labelStyle={INPUT_LABEL}
            placeholder="Company Name"
            value={value}
            onBlur={onBlur}
            onChangeText={onChange}
            autoCompleteType="off"
          />
        )}
        name="companyName"
      />
      <Controller
        control={control}
        render={({ field: { onBlur, onChange, value } }) => (
          <Input
            label="Age"
            labelStyle={INPUT_LABEL}
            placeholder="Age"
            value={value}
            onBlur={onBlur}
            onChangeText={onChange}
            autoCompleteType="cc-number"
            keyboardType="numeric"
          />
        )}
        name="age"
      />
      <Controller
        control={control}
        render={({ field: { onBlur, onChange, value } }) => (
          <Input
            label="Phone Number"
            labelStyle={INPUT_LABEL}
            placeholder="Phone Number"
            value={value}
            onBlur={onBlur}
            onChangeText={onChange}
            autoCompleteType="tel"
            keyboardType="phone-pad"
          />
        )}
        name="phoneNumber"
      />
      <RawText
        style={{
          textAlign: "center",
          marginVertical: 10,
          color: Object.keys(errors).length !== 0 ? color.error : color.transparent,
        }}
        preset="error"
      >
        {errorMessage}
      </RawText>
      <Button preset="largePrimary" title="Sign up" onPress={handleSubmit(handleSignup)} />
      <TextNote>By click button Register, you are considered to accept our License</TextNote>
    </Wrapper>
  )
}
