import React, { useState } from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { ButtonProps } from "../button/button.props"
import { Button } from "../button/button"

const BUTTON_TITLE: TextStyle = {
  fontSize: 10,
}

type props = {
  style?: StyleProp<ViewStyle>
  onChange: (filter) => void
}

const FilterButton = (props: ButtonProps) => <Button titleStyle={BUTTON_TITLE} {...props} />

export function FilterOptions({ style, onChange }: props) {
  const [focus, setFocus] = useState([true, false, false, false])

  return (
    <View
      style={Object.assign(
        {
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-evenly",
        },
        style,
      )}
    >
      <FilterButton
        title="By date of concert"
        preset={focus[0] ? "primary" : "outline"}
        onPress={() => {
          setFocus([true, false, false, false])
          onChange("timeFrom")
        }}
        containerStyle={{ flexGrow: 0, marginRight: 5 }}
      />
      <FilterButton
        title="New"
        preset={focus[1] ? "primary" : "outline"}
        onPress={() => {
          setFocus([false, true, false, false])
          onChange("createdAt")
        }}
        containerStyle={{ flexGrow: 1, marginRight: 5 }}
      />
      <FilterButton
        title="View"
        preset={focus[2] ? "primary" : "outline"}
        onPress={() => {
          setFocus([false, false, true, false])
          onChange("views")
        }}
        containerStyle={{ flexGrow: 1, marginRight: 5 }}
      />
      <FilterButton
        title="Like"
        preset={focus[3] ? "primary" : "outline"}
        onPress={() => {
          setFocus([false, false, false, true])
          onChange("like")
        }}
        containerStyle={{ flexGrow: 1 }}
      />
    </View>
  )
}
