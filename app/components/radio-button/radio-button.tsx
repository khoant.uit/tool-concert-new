import * as React from "react"
import { observer } from "mobx-react-lite"
import { color } from "../../theme"
import { CheckBox, CheckBoxProps } from "react-native-elements"

/**
 * Describe your component here
 */
export const RadioButton = observer(function RadioButton(props: CheckBoxProps) {
  return (
    <CheckBox
      containerStyle={{
        backgroundColor: color.transparent,
        borderColor: color.transparent,
        padding: 0,
        margin: 0,
        marginHorizontal: 0,
      }}
      checkedIcon="dot-circle-o"
      uncheckedIcon="circle-o"
      {...props}
    />
  )
})
