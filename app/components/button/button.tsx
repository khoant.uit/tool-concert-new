import * as React from "react"
import { viewPresets, textPresets } from "./button.presets"
import { ButtonProps } from "./button.props"
import { mergeAll, flatten } from "ramda"
import { Button as RNEButton } from "react-native-elements"
import { TextStyle, ViewStyle } from "react-native"
/**
 * For your text displaying needs.
 *
 * This component is a HOC over the built-in React Native one.
 */
export function Button(props: ButtonProps) {
  // grab the props
  const {
    preset = "primary",
    tx,
    titleStyle: titleStyleOverride,
    buttonStyle: buttonStyleOverride,
    ...rest
  } = props

  const titleStyle = mergeAll(
    flatten([textPresets[preset] || textPresets.primary, titleStyleOverride as TextStyle]),
  )
  const buttonStyle = mergeAll(
    flatten([viewPresets[preset] || viewPresets.primary, buttonStyleOverride as ViewStyle]),
  )

  return <RNEButton titleStyle={titleStyle} buttonStyle={buttonStyle} {...rest} />
}
