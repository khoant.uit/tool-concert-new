import { ViewStyle, TextStyle } from "react-native"
import { color } from "../../theme"

/**
 * All text will start off looking like this.
 */
const BASE_VIEW: ViewStyle = {
  borderWidth: 1,
}

const BASE_TEXT: TextStyle = {
  fontWeight: "700",
}

/**
 * All the variations of text styling within the app.
 *
 * You want to customize these to whatever you need in your app.
 */
export const viewPresets = {
  /**
   * A smaller piece of secondard information.
   */
  primary: { ...BASE_VIEW, backgroundColor: color.primary } as ViewStyle,

  /**
   * A large primary button
   */
  largePrimary: {
    ...BASE_VIEW,
    backgroundColor: color.primary,
    minHeight: 45,
  } as ViewStyle,
  /**
   * A large outline button
   */
  largeOutline: {
    ...BASE_VIEW,
    backgroundColor: color.transparent,
    minHeight: 45,
  } as ViewStyle,
  /**
   * A button without extras.
   */
  link: {
    ...BASE_VIEW,
    paddingHorizontal: 0,
    paddingVertical: 0,
  } as ViewStyle,

  /**
   * A button with outline
   */
  outline: {
    ...BASE_VIEW,
    borderColor: color.primary,
    backgroundColor: color.transparent,
  } as ViewStyle,
}

export const textPresets = {
  primary: { ...BASE_TEXT, color: color.palette.white } as TextStyle,
  largePrimary: {
    ...BASE_TEXT,
    fontSize: 18,
    color: color.palette.white,
  } as TextStyle,
  largeOutline: {
    ...BASE_TEXT,
    fontSize: 18,
    color: color.primary,
  } as TextStyle,
  link: {
    ...BASE_TEXT,
    color: color.text,
    paddingHorizontal: 0,
    paddingVertical: 0,
  } as TextStyle,
  outline: {
    ...BASE_TEXT,
    color: color.text,
  },
}

/**
 * A list of preset names.
 */
export type ButtonPresetNames = keyof typeof viewPresets
