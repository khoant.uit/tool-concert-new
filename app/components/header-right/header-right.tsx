import * as React from "react"
import { Alert, Pressable, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { useAuth } from "../../hooks/useAuth"
import { useNavigation } from "@react-navigation/native"
import { AddIcon } from "../../components"
import { showMessage } from "react-native-flash-message"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

export interface HeaderRightProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
}

/**
 * Describe your component here
 */
export const HeaderRight = observer(function HeaderRight(props: HeaderRightProps) {
  const { style } = props
  const { role } = useAuth()
  const navigation = useNavigation()
  const handleNavigation = () => {
    console.tron.logImportant(role)
    role === "user" &&
      showMessage({
        message: "Premium feature",
        description: "Concert registration is a premium feature, you must paid to use it.",
        type: "info",
      })
    role === "admin" &&
      showMessage({
        message: "Invalid user",
        description: `The logged in user has role ${role}, which has nothing to do with this app.`,
        type: "warning",
      })
    role === "guest" && navigation.navigate("login")
    role === "userPaid" && navigation.navigate("concertRegistration")
  }
  return (
    <Pressable style={[CONTAINER, style]} onPress={handleNavigation}>
      <AddIcon />
    </Pressable>
  )
})
