/**
 * This is the navigator you will modify to display the logged-in screens of your app.
 * You can use RootNavigator to also display an auth flow or other user flows.
 *
 * You'll likely spend most of your time in this file.
 */
import React from "react"
import { createStackNavigator, CardStyleInterpolators } from "@react-navigation/stack"
import { createBottomTabNavigator, BottomTabBarOptions } from "@react-navigation/bottom-tabs"
import {
  WelcomeScreen,
  DemoScreen,
  DemoListScreen,
  HomeScreen,
  TodayConcertScreen,
  MyFavoriteScreen,
  MyPageScreen,
  LoginScreen,
  SignupScreen,
  ConcertRegistrationScreen,
  ConcertDetailScreen,
} from "../screens"
import {
  HeartIcon,
  HeartIconInvert,
  PeopleIcon,
  PeopleIconInvert,
  SearchTabBar,
  SearchTabBarInvert,
  TodayConcertTabBar,
  TodayConcertTabBarInvert,
} from "../components/"
import { color } from "./../theme/color"
/**
 * This type allows TypeScript to know what routes are defined in this navigator
 * as well as what properties (if any) they might take when navigating to them.
 *
 * If no params are allowed, pass through `undefined`. Generally speaking, we
 * recommend using your MobX-State-Tree store(s) to keep application state
 * rather than passing state through navigation params.
 *
 * For more information, see this documentation:
 *   https://reactnavigation.org/docs/params/
 *   https://reactnavigation.org/docs/typescript#type-checking-the-navigator
 */
export type PrimaryParamList = {
  welcome: undefined
  demo: undefined
  demoList: undefined
  home: undefined
  login: undefined
  signup: undefined
  concertRegistration: undefined
  concertDetail: undefined
}

// Documentation: https://reactnavigation.org/docs/stack-navigator/
const Stack = createStackNavigator<PrimaryParamList>()
const Tab = createBottomTabNavigator()

const TABBAROPTIONS: BottomTabBarOptions = {
  labelStyle: {
    fontWeight: "500",
  },
  tabStyle: {
    borderWidth: 0.5,
    borderColor: color.primary,
  },
  style: {
    borderWidth: 0.5,
    borderTopWidth: 1,
    borderColor: color.primary,
  },
  activeTintColor: color.palette.white,
  activeBackgroundColor: color.primary,
}

const HomeTabs = () => (
  <Tab.Navigator tabBarOptions={TABBAROPTIONS}>
    <Tab.Screen
      name={"home"}
      component={HomeScreen}
      options={{
        tabBarIcon: ({ focused }) => (!focused ? <SearchTabBar /> : <SearchTabBarInvert />),
        title: "Home",
      }}
    />
    <Tab.Screen
      name="today-concert"
      component={TodayConcertScreen}
      options={{
        tabBarIcon: ({ focused }) =>
          !focused ? <TodayConcertTabBar /> : <TodayConcertTabBarInvert />,
        title: "Today's Concert",
      }}
    />
    <Tab.Screen
      name="my-favorite"
      component={MyFavoriteScreen}
      options={{
        tabBarIcon: ({ focused }) => (!focused ? <HeartIcon /> : <HeartIconInvert />),
        title: "My Favorite",
      }}
    />
    <Tab.Screen
      name="my-page"
      component={MyPageScreen}
      options={{
        tabBarIcon: ({ focused }) => (!focused ? <PeopleIcon /> : <PeopleIconInvert />),
        title: "My Page",
      }}
    />
  </Tab.Navigator>
)

export function MainNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}
    >
      <Stack.Screen name="welcome" component={WelcomeScreen} />
      <Stack.Screen name="demo" component={DemoScreen} />
      <Stack.Screen name="demoList" component={DemoListScreen} />
      <Stack.Screen name="home" component={HomeTabs} />
      <Stack.Screen name="login" component={LoginScreen} />
      <Stack.Screen name="signup" component={SignupScreen} />
      <Stack.Screen name="concertRegistration" component={ConcertRegistrationScreen} />
      <Stack.Screen name="concertDetail" component={ConcertDetailScreen} />
    </Stack.Navigator>
  )
}

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 *
 * `canExit` is used in ./app/app.tsx in the `useBackButtonHandler` hook.
 */
const exitRoutes = ["welcome"]
export const canExit = (routeName: string) => exitRoutes.includes(routeName)
