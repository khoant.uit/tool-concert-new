import { useContext } from "react"
import { remove, saveString } from "../utils/storage"
import { AuthContext } from "../contexts/AuthContext"

type roleType = "guest" | "user" | "userPaid" | "admin"

const useAuth = () => {
  const [state, setState] = useContext<any>(AuthContext)

  function SIGNIN_SUCCESS({ access_token, role }) {
    saveString("access_token", access_token)
    saveString("role", role)
    setState((state) => ({ ...state, role: role }))
  }

  function SIGNOUT() {
    remove("access_token")
    saveString("role", "guest")
    setState((state) => ({ ...state, role: "guest" }))
  }

  return { SIGNIN_SUCCESS, SIGNOUT, role: state.role as roleType }
}

export { useAuth }
