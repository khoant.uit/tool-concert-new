import gql from "graphql-tag"

gql`
  query categories {
    categories {
      id
      name
    }
  }
`
