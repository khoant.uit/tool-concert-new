import {gql} from '@apollo/client'

gql`
query Users ($id: String!) {
    user(id: $id){
      id
      displayName
      role
    }
  }
`;