import { gql } from "@apollo/client"

gql`
  query concerts($limit: Int, $offset: Int, $orderBy: SortOptionsARG, $searchText: String) {
    concerts(limit: $limit, offset: $offset, orderBy: $orderBy, searchText: $searchText) {
      id
      name
      place
      timeFrom
      timeTo
      views
      categories {
        id
        name
      }
      description
      ownerId
      owner {
        displayName
      }
      backgroundUrl
      photos {
        id
        url
        seq
      }
      performances {
        id
        name
      }
      createdAt
    }
  }
`
