import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A date-time string at UTC, such as 2019-12-03T09:54:33Z, compliant with the date-time format. */
  DateTime: any;
};

export type Category = {
  __typename?: 'Category';
  id: Scalars['Int'];
  name: Scalars['String'];
};

export type Concert = {
  __typename?: 'Concert';
  id: Scalars['Int'];
  name: Scalars['String'];
  place: Scalars['String'];
  timeFrom: Scalars['DateTime'];
  timeTo: Scalars['DateTime'];
  views: Scalars['Float'];
  categories?: Maybe<Array<Category>>;
  description?: Maybe<Scalars['String']>;
  secretKey: Scalars['String'];
  ownerId: Scalars['String'];
  owner: User;
  backgroundUrl?: Maybe<Scalars['String']>;
  photos?: Maybe<Array<ConcertPhoto>>;
  performances?: Maybe<Array<Performance>>;
  createdAt: Scalars['DateTime'];
};

export type ConcertComment = {
  __typename?: 'ConcertComment';
  id: Scalars['Int'];
  text: Scalars['String'];
  user: User;
};

export type ConcertPerformanceJudgeRelations = {
  __typename?: 'ConcertPerformanceJudgeRelations';
  id: Scalars['Int'];
  concertId: Scalars['Int'];
  performanceId: Scalars['Int'];
  judgeId?: Maybe<Scalars['Int']>;
  judgeName: Scalars['String'];
  point: Scalars['Int'];
  rank: Scalars['Int'];
};

export type ConcertPerformanceJudgeRelationsDto = {
  id?: Maybe<Scalars['Int']>;
  concertId: Scalars['Int'];
  judgeId?: Maybe<Scalars['Int']>;
  judgeName: Scalars['String'];
  secretKey: Scalars['String'];
  performances: Array<PerformanceJudgeDto>;
};

export type ConcertPhoto = {
  __typename?: 'ConcertPhoto';
  id: Scalars['Int'];
  url: Scalars['String'];
  seq: Scalars['Float'];
};

export type CreateCategoryDto = {
  name: Scalars['String'];
};

export type CreateCommentDto = {
  text: Scalars['String'];
};

export type CreateConcertDto = {
  name: Scalars['String'];
  place: Scalars['String'];
  timeFrom: Scalars['DateTime'];
  timeTo: Scalars['DateTime'];
  description?: Maybe<Scalars['String']>;
  secretKey: Scalars['String'];
  backgroundUrl?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  categoryIds: Array<Scalars['Int']>;
  performances?: Maybe<Array<PerformanceDto>>;
  photos?: Maybe<Array<PhotoDto>>;
};

export type CreatePerformanceDto = {
  name: Scalars['String'];
  rank?: Maybe<Scalars['Int']>;
};

export type CreatePhotoDto = {
  url: Scalars['String'];
  seq?: Maybe<Scalars['Float']>;
};


export enum Gender {
  Male = 'male',
  Female = 'female'
}

export type Mutation = {
  __typename?: 'Mutation';
  addScoresByJudge: Scalars['Boolean'];
  createComment: Scalars['Boolean'];
  createConcert: Scalars['Boolean'];
  updateConcert: Scalars['Boolean'];
  viewConcert: Scalars['Boolean'];
  deleteConcert: Scalars['Boolean'];
  addPerformanceConcert: Scalars['Boolean'];
  deletePerformanceConcert: Scalars['Boolean'];
  createCategory: Scalars['Boolean'];
  updateCategory: Scalars['Boolean'];
  deleteCategory: Scalars['Boolean'];
  createPerformance: Scalars['Boolean'];
  updatePerformance: Scalars['Boolean'];
  deletePerformance: Scalars['Boolean'];
  createPhoto: Scalars['Boolean'];
  deletePhoto: Scalars['Boolean'];
  register: Scalars['Boolean'];
};


export type MutationAddScoresByJudgeArgs = {
  input: ConcertPerformanceJudgeRelationsDto;
};


export type MutationCreateCommentArgs = {
  input: CreateCommentDto;
  concertId: Scalars['Int'];
};


export type MutationCreateConcertArgs = {
  input: CreateConcertDto;
};


export type MutationUpdateConcertArgs = {
  input: UpdateConcertDto;
  id: Scalars['Int'];
};


export type MutationViewConcertArgs = {
  id: Scalars['Int'];
};


export type MutationDeleteConcertArgs = {
  id: Scalars['Int'];
};


export type MutationAddPerformanceConcertArgs = {
  performanceId: Scalars['Int'];
  concertId: Scalars['Int'];
};


export type MutationDeletePerformanceConcertArgs = {
  performanceId: Scalars['Int'];
  concertId: Scalars['Int'];
};


export type MutationCreateCategoryArgs = {
  input: CreateCategoryDto;
};


export type MutationUpdateCategoryArgs = {
  input: UpdateCategoryDto;
  id: Scalars['Int'];
};


export type MutationDeleteCategoryArgs = {
  id: Scalars['Int'];
};


export type MutationCreatePerformanceArgs = {
  input: CreatePerformanceDto;
};


export type MutationUpdatePerformanceArgs = {
  input: UpdatePerformanceDto;
  id: Scalars['Int'];
};


export type MutationDeletePerformanceArgs = {
  id: Scalars['Int'];
};


export type MutationCreatePhotoArgs = {
  input: CreatePhotoDto;
  concertId: Scalars['Int'];
};


export type MutationDeletePhotoArgs = {
  id: Scalars['Int'];
};


export type MutationRegisterArgs = {
  input: RegisterUserInput;
};

export type Performance = {
  __typename?: 'Performance';
  id: Scalars['Int'];
  name: Scalars['String'];
  rank?: Maybe<Scalars['Int']>;
  points?: Maybe<Array<ConcertPerformanceJudgeRelations>>;
};

export type PerformanceDto = {
  name: Scalars['String'];
};

export type PerformanceJudgeDto = {
  id: Scalars['Int'];
  point: Scalars['Int'];
};

export type PhotoDto = {
  url: Scalars['String'];
  seq?: Maybe<Scalars['Float']>;
};

export type Query = {
  __typename?: 'Query';
  commentsByConcert: Array<ConcertComment>;
  concerts: Array<Concert>;
  concert: Concert;
  categories: Array<Category>;
  user: User;
  users: Scalars['Boolean'];
};


export type QueryCommentsByConcertArgs = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<SortOptionsArg>;
  searchText?: Maybe<Scalars['String']>;
  concertId: Scalars['Int'];
};


export type QueryConcertsArgs = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<SortOptionsArg>;
  searchText?: Maybe<Scalars['String']>;
};


export type QueryConcertArgs = {
  id: Scalars['Int'];
};


export type QueryUserArgs = {
  id: Scalars['String'];
};

export type RegisterUserInput = {
  email: Scalars['String'];
  displayName: Scalars['String'];
  photoURL?: Maybe<Scalars['String']>;
  companyName?: Maybe<Scalars['String']>;
  gender?: Maybe<Gender>;
  age?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  password: Scalars['String'];
};

export enum Role {
  Admin = 'admin',
  UserPaid = 'userPaid',
  User = 'user'
}

export enum SortDirection {
  Desc = 'desc',
  Asc = 'asc'
}

export type SortOptionsArg = {
  field?: Maybe<Scalars['String']>;
  direction?: Maybe<SortDirection>;
};

export type UpdateCategoryDto = {
  name: Scalars['String'];
};

export type UpdateConcertDto = {
  name?: Maybe<Scalars['String']>;
  place?: Maybe<Scalars['String']>;
  timeFrom?: Maybe<Scalars['DateTime']>;
  timeTo?: Maybe<Scalars['DateTime']>;
  description?: Maybe<Scalars['String']>;
  secretKey?: Maybe<Scalars['String']>;
  backgroundUrl?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  categoryIds?: Maybe<Array<Scalars['Int']>>;
};

export type UpdatePerformanceDto = {
  name?: Maybe<Scalars['String']>;
  rank?: Maybe<Scalars['Int']>;
};

export type User = {
  __typename?: 'User';
  id: Scalars['String'];
  email: Scalars['String'];
  displayName: Scalars['String'];
  photoURL?: Maybe<Scalars['String']>;
  role: Role;
  companyName?: Maybe<Scalars['String']>;
  gender?: Maybe<Gender>;
  age?: Maybe<Scalars['Float']>;
  phoneNumber?: Maybe<Scalars['String']>;
  access_token?: Maybe<Scalars['String']>;
  expiresTime?: Maybe<Scalars['Float']>;
};

export type CreateConcertMutationVariables = Exact<{
  input: CreateConcertDto;
}>;


export type CreateConcertMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'createConcert'>
);

export type RegisterUserMutationVariables = Exact<{
  input: RegisterUserInput;
}>;


export type RegisterUserMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'register'>
);

export type CategoriesQueryVariables = Exact<{ [key: string]: never; }>;


export type CategoriesQuery = (
  { __typename?: 'Query' }
  & { categories: Array<(
    { __typename?: 'Category' }
    & Pick<Category, 'id' | 'name'>
  )> }
);

export type ConcertsQueryVariables = Exact<{
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<SortOptionsArg>;
  searchText?: Maybe<Scalars['String']>;
}>;


export type ConcertsQuery = (
  { __typename?: 'Query' }
  & { concerts: Array<(
    { __typename?: 'Concert' }
    & Pick<Concert, 'id' | 'name' | 'place' | 'timeFrom' | 'timeTo' | 'views' | 'description' | 'ownerId' | 'backgroundUrl' | 'createdAt'>
    & { categories?: Maybe<Array<(
      { __typename?: 'Category' }
      & Pick<Category, 'id' | 'name'>
    )>>, owner: (
      { __typename?: 'User' }
      & Pick<User, 'displayName'>
    ), photos?: Maybe<Array<(
      { __typename?: 'ConcertPhoto' }
      & Pick<ConcertPhoto, 'id' | 'url' | 'seq'>
    )>>, performances?: Maybe<Array<(
      { __typename?: 'Performance' }
      & Pick<Performance, 'id' | 'name'>
    )>> }
  )> }
);

export type UsersQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type UsersQuery = (
  { __typename?: 'Query' }
  & { user: (
    { __typename?: 'User' }
    & Pick<User, 'id' | 'displayName' | 'role'>
  ) }
);


export const CreateConcertDocument = gql`
    mutation createConcert($input: CreateConcertDTO!) {
  createConcert(input: $input)
}
    `;
export type CreateConcertMutationFn = Apollo.MutationFunction<CreateConcertMutation, CreateConcertMutationVariables>;

/**
 * __useCreateConcertMutation__
 *
 * To run a mutation, you first call `useCreateConcertMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateConcertMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createConcertMutation, { data, loading, error }] = useCreateConcertMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateConcertMutation(baseOptions?: Apollo.MutationHookOptions<CreateConcertMutation, CreateConcertMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateConcertMutation, CreateConcertMutationVariables>(CreateConcertDocument, options);
      }
export type CreateConcertMutationHookResult = ReturnType<typeof useCreateConcertMutation>;
export type CreateConcertMutationResult = Apollo.MutationResult<CreateConcertMutation>;
export type CreateConcertMutationOptions = Apollo.BaseMutationOptions<CreateConcertMutation, CreateConcertMutationVariables>;
export const RegisterUserDocument = gql`
    mutation registerUser($input: RegisterUserInput!) {
  register(input: $input)
}
    `;
export type RegisterUserMutationFn = Apollo.MutationFunction<RegisterUserMutation, RegisterUserMutationVariables>;

/**
 * __useRegisterUserMutation__
 *
 * To run a mutation, you first call `useRegisterUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerUserMutation, { data, loading, error }] = useRegisterUserMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useRegisterUserMutation(baseOptions?: Apollo.MutationHookOptions<RegisterUserMutation, RegisterUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RegisterUserMutation, RegisterUserMutationVariables>(RegisterUserDocument, options);
      }
export type RegisterUserMutationHookResult = ReturnType<typeof useRegisterUserMutation>;
export type RegisterUserMutationResult = Apollo.MutationResult<RegisterUserMutation>;
export type RegisterUserMutationOptions = Apollo.BaseMutationOptions<RegisterUserMutation, RegisterUserMutationVariables>;
export const CategoriesDocument = gql`
    query categories {
  categories {
    id
    name
  }
}
    `;

/**
 * __useCategoriesQuery__
 *
 * To run a query within a React component, call `useCategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCategoriesQuery({
 *   variables: {
 *   },
 * });
 */
export function useCategoriesQuery(baseOptions?: Apollo.QueryHookOptions<CategoriesQuery, CategoriesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CategoriesQuery, CategoriesQueryVariables>(CategoriesDocument, options);
      }
export function useCategoriesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CategoriesQuery, CategoriesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CategoriesQuery, CategoriesQueryVariables>(CategoriesDocument, options);
        }
export type CategoriesQueryHookResult = ReturnType<typeof useCategoriesQuery>;
export type CategoriesLazyQueryHookResult = ReturnType<typeof useCategoriesLazyQuery>;
export type CategoriesQueryResult = Apollo.QueryResult<CategoriesQuery, CategoriesQueryVariables>;
export const ConcertsDocument = gql`
    query concerts($limit: Int, $offset: Int, $orderBy: SortOptionsARG, $searchText: String) {
  concerts(
    limit: $limit
    offset: $offset
    orderBy: $orderBy
    searchText: $searchText
  ) {
    id
    name
    place
    timeFrom
    timeTo
    views
    categories {
      id
      name
    }
    description
    ownerId
    owner {
      displayName
    }
    backgroundUrl
    photos {
      id
      url
      seq
    }
    performances {
      id
      name
    }
    createdAt
  }
}
    `;

/**
 * __useConcertsQuery__
 *
 * To run a query within a React component, call `useConcertsQuery` and pass it any options that fit your needs.
 * When your component renders, `useConcertsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useConcertsQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      orderBy: // value for 'orderBy'
 *      searchText: // value for 'searchText'
 *   },
 * });
 */
export function useConcertsQuery(baseOptions?: Apollo.QueryHookOptions<ConcertsQuery, ConcertsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ConcertsQuery, ConcertsQueryVariables>(ConcertsDocument, options);
      }
export function useConcertsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ConcertsQuery, ConcertsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ConcertsQuery, ConcertsQueryVariables>(ConcertsDocument, options);
        }
export type ConcertsQueryHookResult = ReturnType<typeof useConcertsQuery>;
export type ConcertsLazyQueryHookResult = ReturnType<typeof useConcertsLazyQuery>;
export type ConcertsQueryResult = Apollo.QueryResult<ConcertsQuery, ConcertsQueryVariables>;
export const UsersDocument = gql`
    query Users($id: String!) {
  user(id: $id) {
    id
    displayName
    role
  }
}
    `;

/**
 * __useUsersQuery__
 *
 * To run a query within a React component, call `useUsersQuery` and pass it any options that fit your needs.
 * When your component renders, `useUsersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUsersQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useUsersQuery(baseOptions: Apollo.QueryHookOptions<UsersQuery, UsersQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<UsersQuery, UsersQueryVariables>(UsersDocument, options);
      }
export function useUsersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<UsersQuery, UsersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<UsersQuery, UsersQueryVariables>(UsersDocument, options);
        }
export type UsersQueryHookResult = ReturnType<typeof useUsersQuery>;
export type UsersLazyQueryHookResult = ReturnType<typeof useUsersLazyQuery>;
export type UsersQueryResult = Apollo.QueryResult<UsersQuery, UsersQueryVariables>;