import { gql } from "@apollo/client"

gql`
  mutation createConcert($input: CreateConcertDTO!) {
    createConcert(input: $input)
  }
`
