import { gql } from "@apollo/client"

gql`
  mutation registerUser($input: RegisterUserInput!) {
    register(input: $input)
  }
`
