import React from "react"
import { View, StyleSheet, Pressable, ViewStyle, TextStyle, Text } from "react-native"
import { SignupForm, BackIcon, Screen } from "../components"
import { Header as RNEHeader } from "react-native-elements"
import { useNavigation } from "@react-navigation/native"
import { color, spacing } from "../theme"

// const SignupHeader = () => (
//     <Header
//         centerComponent={
//             <View>
//                 <Text style={styles.textWelcome}>Sign Up</Text>
//                 <Text style={styles.textSubHeader}>
//                     Create an account to continue
//                 </Text>
//             </View>
//         }
//     />
// );

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
}
const BOLD: TextStyle = { fontWeight: "700" }
const HEADER_TITLE: TextStyle = {
  ...BOLD,
  fontSize: 18,
  lineHeight: 28,
  textAlign: "center",
  color: color.palette.white,
}
const HEADER_SUBTITLE: TextStyle = {
  ...BOLD,
  fontSize: 12,
  textAlign: "center",
  color: color.palette.white,
}

const HeaderComponent = () => (
  <View>
    <Text style={HEADER_TITLE}>Sign up</Text>
    <Text style={HEADER_SUBTITLE}>Create an account to continue</Text>
  </View>
)

export function SignupScreen() {
  const navigation = useNavigation()
  const goBack = () => navigation.goBack()
  return (
    <View testID="SignupScreen" style={FULL}>
      <RNEHeader
        rightContainerStyle={{
          justifyContent: "center",
        }}
        leftContainerStyle={{
          justifyContent: "center",
        }}
        leftComponent={
          <Pressable onPress={goBack}>
            <BackIcon />
          </Pressable>
        }
        centerComponent={<HeaderComponent />}
      />
      <Screen style={CONTAINER} preset="scroll" unsafe>
        <View style={styles.container}>
          <View style={styles.contentContainer}>
            <SignupForm />
          </View>
        </View>
      </Screen>
    </View>
    // <>
    //   <RNEHeader
    //     leftComponent={
    //       <Pressable onPress={goBack}>
    //         <BackIcon />
    //       </Pressable>
    //     }
    //   />
    //   <View style={styles.container}>
    //     <View style={styles.contentContainer}>
    //       <SignupForm />
    //     </View>
    //   </View>
    // </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  textWelcome: {
    fontSize: 18,
    color: "white",
    lineHeight: 28,
    fontWeight: "700",
    textAlign: "center",
  },
  textSubHeader: {
    fontSize: 12,
    color: "white",
    fontWeight: "700",
    textAlign: "center",
  },
  contentContainer: {
    flex: 1,
    alignItems: "center",
    paddingHorizontal: 20,
    marginTop: 30,
  },
})
