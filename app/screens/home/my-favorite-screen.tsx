import React from "react"
import { Pressable, View, ViewStyle } from "react-native"
import { useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"
import { Screen, BackIcon, HeaderRight, Text } from "../../components"
import { color } from "../../theme"
import { Header as RNEHeader } from "react-native-elements"

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
}

export const MyFavoriteScreen = observer(function MyFavoriteScreen() {
  const navigation = useNavigation()
  const backToTodayConcert = () => navigation.navigate("today-concert")

  return (
    <View testID="MyFavoriteScreen" style={FULL}>
      <RNEHeader
        leftComponent={
          <Pressable onPress={backToTodayConcert}>
            <BackIcon />
          </Pressable>
        }
        centerComponent={<Text preset="header">List of Concert</Text>}
        rightComponent={<HeaderRight />}
      />
      <Screen style={CONTAINER} preset="scroll" unsafe></Screen>
    </View>
  )
})
