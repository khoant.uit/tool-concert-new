import React from "react"
import { Pressable, View, ViewStyle } from "react-native"
import { useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"
import { Screen, BackIcon, HeaderRight, Text } from "../../components"
import { color } from "../../theme"
import { Header as RNEHeader } from "react-native-elements"

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
}

export const TodayConcertScreen = observer(function TodayConcertScreen() {
  const navigation = useNavigation()
  const wayBackHome = () => navigation.navigate("home")

  return (
    <View testID="TodayConcertScreen" style={FULL}>
      <RNEHeader
        leftComponent={
          <Pressable onPress={wayBackHome}>
            <BackIcon />
          </Pressable>
        }
        centerComponent={<Text preset="header">List of Concert</Text>}
        rightComponent={<HeaderRight />}
      />
      <Screen style={CONTAINER} preset="scroll" unsafe></Screen>
    </View>
  )
})
