import React, { useEffect } from "react"
import { View, ViewStyle, FlatList } from "react-native"
import { observer } from "mobx-react-lite"
import { Screen, HeaderRight, Text } from "../../components"
import { color, spacing } from "../../theme"
import { Header as RNEHeader } from "react-native-elements"
import { ConcertCard, FilterOptions } from "./../../components"
import { SortDirection, useConcertsQuery } from "../../graphql/gen-types"
import dayjs from "dayjs"
import relativeTime from "dayjs/plugin/relativeTime"
import { useNavigation } from "@react-navigation/native"
import { NetworkStatus } from "@apollo/client"
dayjs.extend(relativeTime)

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
}
// test build app center
export const HomeScreen = observer(function HomeScreen() {
  const navigation = useNavigation()
  const { data, loading, refetch, fetchMore, networkStatus } = useConcertsQuery({
    variables: {
      offset: 0,
      limit: 4,
      orderBy: {
        field: "timeFrom",
        direction: SortDirection.Desc,
      },
    },
    notifyOnNetworkStatusChange: true,
  })
  const concertList = data?.concerts || []
  useEffect(() => {
    navigation.addListener("focus", () => {
      refetch()
    })
  }, [navigation])

  useEffect(() => {
    console.tron.log({ loading })
  }, [loading])

  const handleLoadMoreConcert = () => {
    fetchMore({
      variables: {
        offset: concertList.length,
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev
        return Object.assign({}, prev, {
          concerts: [...prev.concerts, ...fetchMoreResult.concerts],
        })
      },
    })
      .then(({ data }) => data?.concerts.forEach((concert) => console.log(concert.name)))
      .catch((err) => console.log(err))
  }

  return (
    <View testID="HomeScreen" style={FULL}>
      <RNEHeader
        centerComponent={<Text preset="header">List of Concert</Text>}
        rightComponent={<HeaderRight />}
      />
      <Screen style={CONTAINER} preset="fixed" unsafe>
        <FilterOptions
          onChange={(filter) =>
            refetch({
              orderBy: {
                field: filter,
                direction: SortDirection.Desc,
              },
            })
          }
          style={{ paddingVertical: spacing[2] }}
        />
        <FlatList
          data={concertList}
          renderItem={({ item, index, separators }) => {
            const {
              name,
              place,
              timeFrom,
              views,
              owner: { displayName },
              createdAt,
            } = item
            return (
              <ConcertCard
                key={index}
                name={name}
                place={place}
                timeFrom={dayjs(timeFrom).format("HH:mm YYYY/MM/DD")}
                views={views}
                ownerName={displayName}
                createAt={dayjs(createdAt).fromNow()}
              />
            )
          }}
          onEndReached={handleLoadMoreConcert}
          onEndReachedThreshold={0.1}
          keyExtractor={(item) => item.id.toString()}
          refreshing={networkStatus === NetworkStatus.refetch}
          onRefresh={refetch}
        />
      </Screen>
    </View>
  )
})
