import React, { useEffect, useState } from "react"
import { Pressable, Text, View, ViewStyle } from "react-native"
import { useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"
import { Screen, BackIcon, Button, HeaderRight, Text as TextComponent } from "../../components"
import { color } from "../../theme"
import { Header as RNEHeader } from "react-native-elements"
import auth from "@react-native-firebase/auth"
import { useAuth } from "../../hooks/useAuth"
const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
}

export const MyPageScreen = observer(function MyPageScreen() {
  const [initializing, setInitializing] = useState(true)
  const [user, setUser] = useState({ email: "" })
  const navigation = useNavigation()
  const { SIGNOUT } = useAuth()
  function onAuthStateChanged(user) {
    setUser(user)
    if (initializing) setInitializing(false)
  }
  const handleLogin = () => {
    auth()
      .signInWithEmailAndPassword("user@mail.com", "123456")
      .then(async (data) => {
        console.tron.log("login success")
        console.tron.log(data)
        const token = await data.user.getIdToken()
        console.tron.log({ token })
      })
      .catch((error) => {
        console.tron.log({ error })
      })
  }
  const handleSignout = () => {
    auth()
      .signOut()
      .then(() => {
        SIGNOUT()
        navigation.navigate("home")
      })
  }
  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged)
    return subscriber // unsubscribe on unmount
  }, [])
  if (initializing) return null
  if (!user) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Button title="login by paid account" onPress={handleLogin} />
      </View>
    )
  }
  const backToMyFavorite = () => navigation.navigate("my-favorite")

  return (
    <View testID="MyPageScreen" style={FULL}>
      <RNEHeader
        leftComponent={
          <Pressable onPress={backToMyFavorite}>
            <BackIcon />
          </Pressable>
        }
        centerComponent={<TextComponent preset="header">List of Concert</TextComponent>}
        rightComponent={<HeaderRight />}
      />
      <Screen style={CONTAINER} preset="scroll" unsafe>
        <Button title="login by paid account" onPress={handleLogin} />
        <Text>Welcome {user.email}</Text>
        <Button title="signout" onPress={handleSignout} />
      </Screen>
    </View>
  )
})
