import React from "react"
import { StyleSheet, View, Image, Pressable, TextStyle, Text } from "react-native"
import { color } from "../theme"
import { Header as RNEHeader } from "react-native-elements"
import { ViewStyle } from "react-native"
import { BackIcon, Screen, LoginForm } from "../components"
import { useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
}
const BOLD: TextStyle = { fontWeight: "700" }
const HEADER_TITLE: TextStyle = {
  ...BOLD,
  fontSize: 18,
  lineHeight: 28,
  textAlign: "center",
  color: color.palette.white,
}
const HEADER_SUBTITLE: TextStyle = {
  ...BOLD,
  fontSize: 12,
  textAlign: "center",
  color: color.palette.white,
}

const HeaderComponent = () => (
  <View>
    <Text style={HEADER_TITLE}>Welcome Back</Text>
    <Text style={HEADER_SUBTITLE}>Please login to continue</Text>
  </View>
)

export const LoginScreen = observer(function LoginScreen() {
  const navigation = useNavigation()
  const goBack = () => navigation.goBack()

  return (
    <View testID="LoginScreen" style={FULL}>
      <RNEHeader
        rightContainerStyle={{
          justifyContent: "center",
        }}
        leftContainerStyle={{
          justifyContent: "center",
        }}
        centerComponent={<HeaderComponent />}
        leftComponent={
          <Pressable onPress={goBack}>
            <BackIcon />
          </Pressable>
        }
      />
      <Screen style={CONTAINER} preset="scroll" unsafe>
        <View style={styles.container}>
          <View style={styles.loginContainer}>
            <Image
              source={{
                uri:
                  "https://vsudo.net/blog/wp-content/uploads/2020/08/y-tuong-thiet-ke-logo-696x688.jpg",
              }}
              style={styles.logo}
              resizeMode="cover"
            />
            <LoginForm />
          </View>
        </View>
      </Screen>
    </View>
  )
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  textWelcome: {
    fontSize: 18,
    color: "white",
    lineHeight: 28,
    fontWeight: "700",
    textAlign: "center",
  },
  textSubHeader: {
    fontSize: 12,
    color: "white",
    fontWeight: "700",
    textAlign: "center",
  },
  loginContainer: {
    flex: 1,
    alignItems: "center",
    paddingHorizontal: 10,
  },
  logo: {
    width: 150,
    height: 150,
    borderRadius: 75,
    borderColor: color.primary,
    borderWidth: 1,
    marginTop: 30,
    marginBottom: 30,
  },
})
