import React from "react"
import { observer } from "mobx-react-lite"
import { Pressable, View, ViewStyle } from "react-native"
import { BackIcon, HeaderRight, Screen, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"
import { useNavigation } from "@react-navigation/native"
import { Header as RNEHeader } from "react-native-elements"

const FULL: ViewStyle = {
  flex: 1,
}
const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

export const ConcertDetailScreen = observer(function ConcertDetailScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()
  const goBack = () => navigation.goBack()
  return (
    <View style={FULL}>
      <RNEHeader
        centerComponent={<Text preset="header">Concert Detail</Text>}
        leftComponent={
          <Pressable onPress={goBack}>
            <BackIcon />
          </Pressable>
        }
      />
      <Screen style={ROOT} preset="scroll">
        <Text preset="header" text="" />
      </Screen>
    </View>
  )
})
