import React from "react"
import { observer } from "mobx-react-lite"
import { Pressable, TextStyle, View, ViewStyle } from "react-native"
import { BackIcon, ConcertForm, Screen, TickIcon } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"
import { Header as RNEHeader } from "react-native-elements"
import { useNavigation } from "@react-navigation/native"
import { useForm } from "react-hook-form"
import { ConcertsDocument, useCreateConcertMutation } from "../../graphql/gen-types"
import { showMessage } from "react-native-flash-message"

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
}
const BOLD: TextStyle = { fontWeight: "700" }

const HEADER_TITLE: TextStyle = {
  ...BOLD,
  fontSize: 18,
  lineHeight: 28,
  textAlign: "center",
  color: color.palette.white,
}

export const ConcertRegistrationScreen = observer(function ConcertRegistrationScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()
  const goBack = () => navigation.goBack()
  const wayBackHome = () => navigation.navigate("home")
  const { control, handleSubmit, getValues } = useForm()
  const [createConcert] = useCreateConcertMutation({
    onCompleted: (data) => {
      console.tron.log(data)
      showMessage({ message: "Add concert successfully", type: "success" })
      wayBackHome()
    },
    onError: (err) => {
      console.tron.log(err)
      showMessage({ message: "Error!", description: err.message, type: "danger" })
    },
  })
  const onSubmit = (data) => {
    console.tron.log(data)
    const {
      name,
      place,
      timeFrom,
      timeTo,
      categoryIds: { value },
      description,
      secretKey,
    } = data
    createConcert({
      variables: {
        input: {
          name,
          place,
          timeFrom,
          timeTo,
          categoryIds: value,
          description,
          secretKey,
        },
      },
    })
  }

  return (
    <View testID="ConcertRegistrationScreen" style={FULL}>
      <RNEHeader
        leftComponent={
          <Pressable onPress={goBack}>
            <BackIcon />
          </Pressable>
        }
        centerComponent={{ text: "Concert Registration", style: HEADER_TITLE }}
        rightComponent={
          <Pressable onPress={handleSubmit(onSubmit)}>
            <TickIcon />
          </Pressable>
        }
      />
      <Screen style={CONTAINER} preset="scroll" unsafe>
        <ConcertForm control={control} getValues={getValues} />
      </Screen>
    </View>
  )
})
