export const palette = {
  black: "#1d1d1d",
  white: "#ffffff",
  offWhite: "#e6e6e6",
  orange: "#FBA928",
  orangeDarker: "#EB9918",
  grey: "#707070",
  lightGrey: "#A9A9A9",
  lighterGrey: "#CDD4DA",
  angry: "#dd3333",
  brown: "#BFA17B",
  brownLighter: "rgba(228, 195, 154, 0.1)",
}
